# fxdm's academic website

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/) 

Source code of the academic website [fxdm.org](http://www.fxdm.org/) of F.-X. Dechaume-Moncharmont. 

## Content of the repository

This repository contains the actual HTML or CSS files that are running on the server, and (more importantly) the source codes used to generate this files with [Hugo](https://gohugo.io/), an open source static site generator written in Go. The website contents are written in Markdown (.md files). The theme used is adapted from [Arabica](https://github.com/nirocfz/arabica).

## Privacy policy

Privacy Matters. This website does not use any tracking technology, such as cookies, trackers, IP logging or analytics tools. We do not collect, use or store any personal data from the users of this website.

## Building locally

To work locally with this project :
1. Fork, clone or download this project
1. [Install Hugo](https://gohugo.io/installation/)
1. Add content
1. Preview the project: `hugo server`. The site can be accessed under `localhost:1313/hugo/`.
1. Generate the website: `hugo` (optional).
1. Additional information in [Hugo documentation](https://gohugo.io/documentation/)




---
title: ""
date: 2022-11-28T14:02:19+01:00
draft: false
type: _default
layout: single
---
____
\
![fxdm](/img/trombinoscope/fxdm_kerg.jpg)
\
François-Xavier Dechaume-Moncharmont\
Professor of behavioural ecology\
[Université Claude-Bernard Lyon 1](https://www.univ-lyon1.fr/)\
[CNRS UMR 5023 LEHNA](http://umr5023.univ-lyon1.fr/)\
[Research group: Ecophysiology, behaviour and conservation](http://umr5023.univ-lyon1.fr/equipes-de-recherche/e2c)



:envelope: LEHNA, Bât. Darwin C, 6 rue Raphaël Dubois, 69622 Villeurbanne Cedex, France\
Tel: (+33) 4 72 43 13 30,  E-mail: fx.dechaume@univ-lyon1.fr\
:office: [office 12.032](https://pad.lamyne.org/s/TH5wfMHnM), LEHNA, Bât. Darwin C

[![scholar](/img/logo/googlescholar.png)](https://scholar.google.com/citations?hl=en&user=ge366NsAAAAJ) [![orcid](/img/logo/orcid.png)](http://orcid.org/0000-0001-7607-8224) [![orcid](/img/logo/hal_BW.png)](https://cv.hal.science/francois-xavier-dechaume-moncharmont) [![wos](/img/logo/clarivate.png)](http://www.webofscience.com/wos/author/record/F-2394-2011) [![researchgate](/img/logo/researchgate.png)](https://www.researchgate.net/profile/Francois_Xavier_Dechaume-Moncharmont2) [![gitlab](/img/logo/gitlab.png)](https://gitlab.com/fxdm) [![mastodon](/img/logo/mastodon.png)](https://mamot.fr/@fxdm)
____

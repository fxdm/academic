---
title: "Research"
date: 2022-11-28T14:02:19+01:00
draft: false
---

#### Research interest

I am a behavioural ecologist. I am studying animal behaviour from an ecological and evolutionary perspective. More specifically, my research interests include decision rules, rationality, mate choice, sexual selection, foraging strategy, parental care, animal personality, cognitive bias and emotion. So far, I had the opportunity to work on a wide range of species: insect (honeybee, ant, carabid, *Tenebrio*, parasitoid wasp), crustacean (*Gammarus pulex*, woodlouse), fish (convict cichlid, round goby, *Zingel asper*), and bird (tropical dove, Arctic sandpiper). In addition to this experimental work, I indulge myself in theoretical approaches. From a taxonomic viewpoint, my favourite species of models are related to game theory, dynamic programming, and individual based simulations. Finally, I am also interested in statistical methodology (survival model, discriminant analysis, model selection).

[![honeybee](/img/species/honeybee.jpg)](http://www.fxdm.org/download/dechaume-moncharmont_05_hidden_548319.pdf) [![zenaida_dove_CC_siblet](/img/species/zenaida_dove_CC_siblet.jpg)](http://www.fxdm.org/download/dechaume_2011_auk.pdf) [![gammarus_pulex_CC_francois_graf](/img/species/gammarus_pulex_CC_francois_graf.jpg)](http://www.fxdm.org/download/galipaud_13_assortative_864896.pdf) [![harpalus_affinis_CC_entomart](/img/species/harpalus_affinis_CC_entomart.jpg)](http://www.fxdm.org/download/charalabidis_17_risk_1122570.pdf) [![amatitliana_siquia](/img/species/amatitliana_siquia.jpg)](http://advances.sciencemag.org/content/2/3/e1501013.full)  ![apron_CC_erimouche](/img/species/apron_CC_erimouche.jpg) [![goby_CC_mond_van_zwartbekgrondel](/img/species/goby_CC_mond_van_zwartbekgrondel.jpg)](http://www.fxdm.org/download/fernandez-declerck_23_adding.pdf) [![sanderling_CC_jeroen_reneerkens](/img/species/sanderling_CC_jeroen_reneerkens.jpg)](http://www.fxdm.org/download/etchart_24_extended.pdf) [![chevalier](/img/species/chevalier.jpg)](http://www.fxdm.org/download/chevalier_20_fluctuating.pdf) [![galipaud](/img/species/galipaud.jpg)](http://www.fxdm.org/download/galipaud_2014_MEE.pdf)

#### PhD students 	

![Chloé Souques](/img/trombinoscope/chloe_souques.jpg)\
**Chloé Souques** (2023-2026) [[webpage]](https://umr5023.univ-lyon1.fr/annuaire/details/1/513)\
Impact of thermal stochasticity on the physiological, behavioural and ecological response of freshwater fish\
Supervisors: F.-X. Dechaume-Moncharmont, Yann Voituron and Loic Teulier

![Julien Bouvet](/img/trombinoscope/julien_bouvet.jpg)\
**Julien Bouvet** (2022-2025)\
Modelling adaptive parental care strategies in Arctic shorebird\
Supervisors: F.-X. Dechaume-Moncharmont and Jérôme Moreau.

![Chloé Laubu](/img/trombinoscope/chloe_laubu.jpg)\
**Chloé Laubu** (2015-2018) [[webpage]](https://sites.google.com/view/chloelaubu)\
Adaptive value of personality and emotional states: heuristics and decision making in sexual context in a monogamous fish\
Supervisors: F.-X. Dechaume-Moncharmont and Philippe Louâpre.

![Alice Charalabidis](/img/trombinoscope/alice_charalabidis.jpg)\
**Alice Charalabidis** (2014-2017)\
Effect of inter-individual variability and intraguild interferences on the foraging strategies of seed-eating carabid species\
Supervisors: David Bohan, Sandrine Petit and F.-X. Dechaume-Moncharmont.

![Matthias Galipaud](/img/trombinoscope/matthias_galipaud.jpg)\
**Matthias Galipaud** (2009-2012)\
Mating strategies and resulting patterns in mate guarding crustaceans: an empirical and theoretical approach\
Supervisors: Loïc Bollache and F.-X. Dechaume-Moncharmont.

![Karine Monceau](/img/trombinoscope/karine_monceau.jpg)\
**Karine Monceau** (2006-2009) [[webpage]](http://www.kmonceau.fr/)\
Ecology of the Zenaida dove in the Barbados Island\
Supervisors: Frank Cézilly and F.-X. Dechaume-Moncharmont.\
Current position: lecturer in the university of La Rochelle and Centres d’Etudes Biologiques de Chizé (CEBC)


## Video illustration

#### Judgement bias test in Convict cichlid 	

[![laubu_19_fish_open_box](/img/video/laubu_19_fish_open_box.jpg)](https://www.youtube.com/watch?v=gkX5L11K6Oo)\
[![laubu_19_jbt](/img/video/laubu_19_jbt.jpg)](https://www.youtube.com/watch?v=sEBWBbmOruY)\
Convict cichlids (*Amatitlania siquia*) are trained to open boxes covered by a movable lid. The set-up is used in judgement bias test in order to assess mood (emotional state estimated from optimism-pessimism bias) of the fish when separated from their sexual partners.

> Laubu C, Louâpre P, Dechaume-Moncharmont F-X (2019) Pair-bonding influences affective state in a monogamous fish species. Proceedings of the Royal Society B: Biological Sciences 286:20190760 [\[pdf\]](http://www.fxdm.org/download/laubu_19_pairbonding_24343246.pdf) + [\[media coverage\]](https://docs.google.com/document/d/e/2PACX-1vS_ejHoQYyG7v07zTUcx7Z5GSgO1ZV7b30-yo8IMlJqsoZkGVHG0lcNshZok0_Gjw3q0-6r-DRXjcnH/pub)

#### Coordinated nest defense in Convict cichlid 	

[![photo_videoxiloa](/img/video/photo_videoxiloa.jpg)](https://youtu.be/4BuYwqQWGns)\
Cordinated nest defense against fry predators by a pair of Convict cichlid (*Amatitlania siquia*), a monogamous tropical fish species. The video was recorded in Lake Xiloà, Nicaragua. In natural environment, the eggs and young are highly vulnerable to predation from other fish species or even convict cichlids (cannibalism). Both parents perform intensive coordinated defense of the nest entrance in order to repel intruders sometimes much larger than themselves.

> Laubu C., Dechaume-Moncharmont F.-X., Motreuil S., Schweitzer C. (2016) Mismatched partners that achieve postpairing behavioral similarity improve their reproductive success. Science Advances, 2(3):e1501013 [\[open access\]](http://advances.sciencemag.org/content/2/3/e1501013.full) [\[media coverage\]](https://docs.google.com/document/d/1pJZEj0f4EeZroMEF0y_ZwSQVcFYB6TNxhkcVp9srzbY/pub)
 
#### Double amplexus in *Gammarus pulex* 

[![double_amplexus](/img/video/double_amplexus.jpg)](https://youtu.be/ECYCZvMCZEo)\
In *Gammarus pulex*, the pair formation is characterized by a long-lasting mate guarding behaviour, named amplexus, in which a male grasps a female for up to 20 days until the sexual receptiviy of the female. In this video recorded in laboratory conditions, a male is attempting to handle two females, possibly in order to compare the two mating options. 

> Galipaud M., Bollache L., Oughadou A., Dechaume-Moncharmont F.-X. (2015) Males do not always switch females when presented to a better reproductive option. Behavioral Ecology 26:359–366 [\[pdf\]](http://www.fxdm.org/download/Galipaud_2015_males_do_not_always_switch_females.pdf)

 
## Social media 

My tools are open source. Follow me on **Mastodon social**: [@fxdm@mamot.fr](https://mamot.fr/@fxdm)
 
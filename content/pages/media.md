---
title: "Media coverage"
date: 2024-02-23T14:02:19+01:00
draft: false
---

[<img src="/img/coverage_embedded_img/media/2023_arte_01.png"
style="border:1px solid black;height:1.8in"/>](https://myvideo.univ-lyon1.fr/permalink/v12688b5e5c02fg2wn40/iframe/) [<img src="/img/coverage_embedded_img/media/2023_arte_02.png"
style="border:1px solid black;height:1.8in"/>](https://myvideo.univ-lyon1.fr/permalink/v12688b5e5c02fg2wn40/iframe/)
**Arte TV** (France & Germany, December 2023) [French version](https://myvideo.univ-lyon1.fr/permalink/v12688b5e5c02fg2wn40/iframe/)

[<img src="/img/coverage_embedded_img/media/cnrs_journal_2023.png"
style="border:1px solid black;width:4in"/>](https://lejournal.cnrs.fr/articles/les-animaux-des-etres-sensibles)
**CNRS** (France, August 2023) [Online version](https://lejournal.cnrs.fr/articles/les-animaux-des-etres-sensibles)

[<img src="/img/coverage_embedded_img/media/cs014.jpg"
style="border:1px solid black;width:2.5in"/>](https://carnetsdescience-larevue.fr/boutique/mook/carnets-de-science-14/)
**Carnets de sciences** (France, may 2023) [Pdf version](/img/coverage_embedded_img/media/lecomte_23_sensibles.pdf)

[<img src="/img/coverage_embedded_img/media/image99.png"
style="border:1px solid black;width:4in"/>](https://www.rts.ch/play/tv/la-science-des-coeurs-brises/video/peine-de-coeur-dans-laquarium?urn=urn%3Arts%3Avideo%3A13637542)
**RTS, Radio Television Suisse** (Switzerland, December 2022) [Online version](https://www.rts.ch/play/tv/la-science-des-coeurs-brises/video/peine-de-coeur-dans-laquarium?urn=urn%3Arts%3Avideo%3A13637542)

<img src="/img/coverage_embedded_img/media/image25.png"
style="width:2in" />
[<img src="/img/coverage_embedded_img/media/image80.png"
style="border:1px solid black;width:4in" />](https://www.laliberte.ch/news/magazine/societe/la-rts-s-interesse-aux-coeurs-brises-dans-une-serie-instagram-669712)
**La Liberté** (Switzerland, December 2022) [Online version](https://www.laliberte.ch/news/magazine/societe/la-rts-s-interesse-aux-coeurs-brises-dans-une-serie-instagram-669712)

<img src="/img/coverage_embedded_img/media/image79.png"
style="width:2in" />
[<img src="/img/coverage_embedded_img/media/image108.png"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/170M1A8pCe7mzUu_bVQ_jDQXvcO0RVG0g/view?usp=sharing)
**Le Progrès** (France, November 2022) [Printed version](https://drive.google.com/file/d/170M1A8pCe7mzUu_bVQ_jDQXvcO0RVG0g/view?usp=sharing)

[<img src="/img/coverage_embedded_img/media/image57.png"
style="border:1px solid black;width:4in" />](https://popsciences.universite-lyon.fr/ressources/du-mouton-au-poisson-comment-determiner-les-emotions-dun-animal/)
**Pop’Sciences**, podcast (France, Janvier 2022) [Webpage](https://popsciences.universite-lyon.fr/ressources/du-mouton-au-poisson-comment-determiner-les-emotions-dun-animal/)

[<img src="/img/coverage_embedded_img/media/image72.jpg"
style="width:1.5in" />
<img src="/img/coverage_embedded_img/media/image89.png"
style="border:1px solid black;width:4in" />](https://youtu.be/Fh056P1hFRA)
**France TV**, France 3, Entre-deux** (France, October 2021) [Video](https://youtu.be/Fh056P1hFRA)

[<img src="/img/coverage_embedded_img/media/image123.png"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/1o4rP48YG_vfq0A97P6QtuXa9D8dSCPGd/view?usp=sharing)
**Chercheurs d’eau** (France, July 2021) [Printed version](https://drive.google.com/file/d/1o4rP48YG_vfq0A97P6QtuXa9D8dSCPGd/view?usp=sharing)

<img src="/img/coverage_embedded_img/media/image68.jpg" style="border:1px solid black;height:1.4in" /><img src="/img/coverage_embedded_img/media/image128.jpg" style="border:1px solid black;height:1.4in" />
**E=M6** (France, February 2021) 

<img src="/img/coverage_embedded_img/media/image16.jpg"
style="border:1px solid black;width:4in" />
**Welt** (Germany, July 2020) [Online version](https://www.welt.de/kmpkt/article195117895/Liebeskummer-Auch-Fische-leiden-wenn-sie-vom-Partner-getrennt-sind.html)

[<img src="/img/coverage_embedded_img/media/image96.png"
style="border:1px solid black;height:2in" /><img src="/img/coverage_embedded_img/media/image6.png"
style="border:1px solid black;height:2in" />](https://drive.google.com/open?id=1uq-eNtNkn30jbvziCSlar0JkzPZ1N6zO)
**Causette** (France, March 2020) [Printed version](https://drive.google.com/open?id=1uq-eNtNkn30jbvziCSlar0JkzPZ1N6zO)

<img src="/img/coverage_embedded_img/media/image48.png"
style="border:1px solid black;width:2.5in" />
**Muséum des arts et métiers** (Paris, France, February 2020), Museum exhibition about experimental setup

<img src="/img/coverage_embedded_img/media/image85.png"
style="border:1px solid black;width:2.5in" />
**The Cichlid Stage** (USA, February 2020) [Online version](https://thecichlidstage.com/drs-chloe-laubu-and-dechaume-moncharmont-interview/)

<img src="/img/coverage_embedded_img/media/image69.png"
style="width:1.5in"
alt="logo de Groupe Sipa - Ouest-France" />
[<img src="/img/coverage_embedded_img/media/image11.jpg"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/1nBSaAeEGb702mjXFqkxcrkOpjaSCntqf/view?usp=sharing)
**Ouest France** (France, February 2020) [Printed version](https://drive.google.com/file/d/1nBSaAeEGb702mjXFqkxcrkOpjaSCntqf/view?usp=sharing)

[<img src="/img/coverage_embedded_img/media/image93.png"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/1bM4GgL1UX3rl4Z137I0z5362AoJ6NfDU/view?usp=sharing)
**Science du Monde** (France, February 2020) [Printed version](https://drive.google.com/file/d/1bM4GgL1UX3rl4Z137I0z5362AoJ6NfDU/view?usp=sharing)

[<img src="/img/coverage_embedded_img/media/image98.png"
style="border:1px solid black;width:4in" />](https://www.youtube.com/watch?v=Y-CeLd87q5w)
**Zeste de science** (CNRS, France, January 2020) [youtube channel](https://www.youtube.com/watch?v=Y-CeLd87q5w)

[<img src="/img/coverage_embedded_img/media/turak20.jpg"
style="border:1px solid black;width:4in" />](/img/coverage_embedded_img/media/turak20.pdf)
**Art and science:** Theatre performance about Chloé's PhD work by Chloé and and her father
Le Progrès (February 2020) [Printed version](/img/coverage_embedded_img/media/turak20.pdf)

[<img src="/img/coverage_embedded_img/media/image107.png"
style="border:1px solid black;height:2in" /><img src="/img/coverage_embedded_img/media/image95.png"
style="border:1px solid black;height:2in" />](https://drive.google.com/file/d/1CKhfO3_9VfBeTsnsN3jAcgr8TKQDyVVB/view?usp=sharing)
**Science & Vie** (France, December 2019) [printed version](https://drive.google.com/file/d/1CKhfO3_9VfBeTsnsN3jAcgr8TKQDyVVB/view?usp=sharing)

[<img src="/img/coverage_embedded_img/media/image86.png"
style="border:1px solid black;width:4in" />](https://www.rts.ch/info/sciences-tech/environnement/10501297-les-poissons-aussi-ont-un-attachement-emotionnel-envers-leur-moitie.html)
**RTS** (Switzerland, 2019) [online version](https://www.rts.ch/info/sciences-tech/environnement/10501297-les-poissons-aussi-ont-un-attachement-emotionnel-envers-leur-moitie.html)

<img src="/img/coverage_embedded_img/media/image76.jpg"
style="border:1px solid black;width:4in"
alt="https://upload.wikimedia.org/wikipedia/en/3/33/Newshour_%28BBC_World_Service%29_cover_art.jpg" />
**BBC** (United Kingdom, june 2019)
Newhours (interview wirth Tim Franks)

<img src="/img/coverage_embedded_img/media/image100.png"
style="border:1px solid black;width:4in" />
**BBC** (United Kingdom, june 2019)
Newsroom (interview with Valerie Sanderson)

<img src="/img/coverage_embedded_img/media/image36.png"
style="width:1.5in" alt="RTBF" />
**RTBF** (Belgium, 2019)
\- Soir Première (Odile Leherte): radio interview
\- [web page](https://www.rtbf.be/info/insolites/detail_un-poisson-tropical-qui-ressent-le-chagrin-d-amour?id=10243939)

[<img src="/img/coverage_embedded_img/media/image91.png"
style="width:1.5in"
alt="CNN International" />](https://edition.cnn.com/2019/06/12/europe/lovesick-fish-intl-scli-scn-trnd/index.html)
**CNN** (USA) Amy Woodyatt [interview](https://edition.cnn.com/2019/06/12/europe/lovesick-fish-intl-scli-scn-trnd/index.html)

<img src="/img/coverage_embedded_img/media/image114.png"
style="width:1.5in" alt="France Bleu" />
**France Bleue** (France, 2019)
\- Une heure en France (Frédérique Le Teurnier et Denis Faroud): [radio interview](https://www.francebleu.fr/emissions/une-heure-en-france/une-heure-en-france-frederique-leteurnier-denis-faroud-182)
\- Les idées reçues (Damien Mestre): [article](https://www.francebleu.fr/infos/climat-environnement/trois-idees-recues-sur-le-chagrin-amoureux-des-poissons-1560877954)

<img src="/img/coverage_embedded_img/media/image111.jpg"
style="width:1.5in" alt="Médiamétrie / été 2018" />
**France Inter** (France, 2019)
\- La tête au carré (Axel Villard, [interview](https://www.franceinter.fr/emissions/la-une-de-la-science/la-une-de-la-science-14-juin-2019))
\- Par Jupiter (Charline Vanhoenacker, Alex Vizorek): satirical chronicle
\- Matinale (Mathieu Vidard) : scientific review

<img src="/img/coverage_embedded_img/media/image72.jpg"
style="width:1.5in"
alt="France 3 reportage" />
[<img src="/img/coverage_embedded_img/media/france3_19.png"
style="border:1px solid black;width:4in"
alt="France 3 reportage" />](https://www.youtube.com/watch?v=f_m1jMVQKzg)
**France 3 TV** (France)
\- National edition (Journal 13h, 18th June 2019) : [television interview and report](https://www.youtube.com/watch?v=f_m1jMVQKzg)
\- Local edition (journal 19h, 17th June 2019): [television interview and report](https://www.youtube.com/watch?v=Zf_zTQE-6ds)

<img src="/img/coverage_embedded_img/media/image49.png"
style="border:1px solid black;width:4in" />
**Radio Canada** (Canada) [Interview](https://ici.radio-canada.ca/premiere/emissions/bien-entendu/segments/entrevue/122158/cichlide-zebre-poisson-chagrin-amour)

<img src="/img/coverage_embedded_img/media/image115.png"
style="border:1px solid black;width:4in" />
**The Guardian** (UK, 2019)
\- Ian Sample Lovelorn fish have gloomier outlook, study finds ([article](https://www.theguardian.com/science/2019/jun/12/lovelorn-fish-have-gloomier-outlook-study-finds))
\- John Crace A dismal wet week (Tuesday) 

<img src="/img/coverage_embedded_img/media/image43.png"
style="border:1px solid black;width:4in" />
**The Times** (UK, 2019)

[<img src="/img/coverage_embedded_img/media/image110.png"
style="border:1px solid black;width:4in" />](https://tinyurl.com/y63ujwon)
**BFM TV** (France) [Daily news (Matinale, Adeline François, Christophe Delay)](https://tinyurl.com/y63ujwon)

[<img src="/img/coverage_embedded_img/media/image82.png"
style="border:1px solid black;width:4in" />](https://tinyurl.com/y5lk9lwt)
**LCI** (France, 2019) [Daily news (Matinale, Pascale de La Tour du Pin, Benjamin Cruard)](https://tinyurl.com/y5lk9lwt)

[<img src="/img/coverage_embedded_img/media/image24.png"
style="border:1px solid black;width:4in" />](https://www.youtube.com/watch?v=KHKQWX2xGEU)
**RTL** (France) [Daily news (Matinale Yves Cavi, Cyprien Cini)](https://www.youtube.com/watch?v=KHKQWX2xGEU)

<img src="/img/coverage_embedded_img/media/image88.png"
style="border:1px solid black;width:2in" />
**Radio Campus** (France)
\- [Revue de presse scientifique](https://soundcloud.com/universit-de-bourgogne/revue-de-presse-scientifique-19062019)
\- [Chronique de Lionel Maillot](https://soundcloud.com/lionel-maillot/lhistoire-de-chloe)
\- [UniversCité](https://www.mixcloud.com/radiocampusbezak/les-poissons-aussi-ont-des-peines-de-coeur-retour-sur-la-global-game-jam-universcit%C3%A9-250619/)

[<img src="/img/coverage_embedded_img/media/image87.png"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/11yR00U215Hs6Sgg1HvnpK0ILn_Xjx3-T/view)
**Die Zeit** (Germany) [Printed version](https://drive.google.com/file/d/11yR00U215Hs6Sgg1HvnpK0ILn_Xjx3-T/view)

[<img src="/img/coverage_embedded_img/media/image101.jpg"
style="width:2in"
alt="Logo-Le-Figaro" />](https://drive.google.com/file/d/1LXOk5ePH9ZJZ8ytuhg0x3Gw4cOLXXjug/view)
[<img src="/img/coverage_embedded_img/media/figaro19s.jpg"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/1LXOk5ePH9ZJZ8ytuhg0x3Gw4cOLXXjug/view)
**Le Figaro** (France) [Printed version](https://drive.google.com/file/d/1LXOk5ePH9ZJZ8ytuhg0x3Gw4cOLXXjug/view)

[<img src="/img/coverage_embedded_img/media/image119.png"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/13SeEB4DQVkNKf9j0yW8ESD5OBwG1GpIr/view)
**Südwest Presse** (Germany) [Front page](https://drive.google.com/file/d/13SeEB4DQVkNKf9j0yW8ESD5OBwG1GpIr/view)
also in front page of:
**- Reutlinger Nachrichten**
**- Märkische Oderzeitung (Frankfurt, Bad Freienwalde, etc.)**
**- Geisinger Zeitung**
**- Bietigheimer Zeitung**
**- Göppinger Kreisnnachrichten**
**- Hohenloher Tagblatt**
**- Haller Tagblatt**
**- Heidenheimer Neue Presse**
**- Alb Blote (Müsingen)**
**- Rundschau für den Schwäbischen Wald**

[<img src="/img/coverage_embedded_img/media/image117.png"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/1z8jNIB6jqw_hJgFEgioyUHaWdNkfg0pv/view)
**Italia Oggi** (Italy) [Printed version](https://drive.google.com/file/d/1z8jNIB6jqw_hJgFEgioyUHaWdNkfg0pv/view)

[<img src="/img/coverage_embedded_img/media/image51.png"
style="border:1px solid black;width:4in" />](https://drive.google.com/file/d/1kCzgSj1L-FK_SauXYUtj7tVkpjfFmlKz/view)
**Le Bien Public** (France, 2019) [Printed version](https://drive.google.com/file/d/1kCzgSj1L-FK_SauXYUtj7tVkpjfFmlKz/view)

[<img src="/img/coverage_embedded_img/media/image28.png"
style="border:1px solid black;width:4in" />](https://www.lepoint.fr/insolite/ce-poisson-peut-ressentir-un-chagrin-d-amour-12-06-2019-2318526_48.php)
**Le Point** (France) [Online version](https://www.lepoint.fr/insolite/ce-poisson-peut-ressentir-un-chagrin-d-amour-12-06-2019-2318526_48.php)

[<img src="/img/coverage_embedded_img/media/image60.png"
style="border:1px solid black;width:4in" />](https://www.huffingtonpost.es/entry/los-peces-tambien-sufren-el-desamor_es_5d084c0be4b09532783910c4)
**Huffington Post** (Spain) [Online version](https://www.huffingtonpost.es/entry/los-peces-tambien-sufren-el-desamor_es_5d084c0be4b09532783910c4)

[<img src="/img/coverage_embedded_img/media/image10.png"
style="border:1px solid black;width:4in" />](https://www.journaldemontreal.com/2019/06/11/les-poissons-aussi-ont-des-chagrins-damour-1)
**Journal de Montréal** (Canada) [Online version](https://www.journaldemontreal.com/2019/06/11/les-poissons-aussi-ont-des-chagrins-damour-1)

[<img src="/img/coverage_embedded_img/media/image104.png"
style="border:1px solid black;width:4in" />](https://www.heidi.news/articles/les-poissons-aussi-perdent-le-moral-apres-un-chagrin-d-amour)
**Heidi News** (Switzerland)
[Online version](https://www.heidi.news/articles/les-poissons-aussi-perdent-le-moral-apres-un-chagrin-d-amour)

[<img src="/img/coverage_embedded_img/media/image83.png"
style="border:1px solid black;width:4in" />](https://www.stern.de/neon/feierabend/forscher--fische-koennen-liebeskummer-bekommen-8750676.html)
**Stern** (Germany) [Online version](https://www.stern.de/neon/feierabend/forscher--fische-koennen-liebeskummer-bekommen-8750676.html)

[<img src="/img/coverage_embedded_img/media/image74.png"
style="border:1px solid black;width:4in" />](https://www.vox.com/future-perfect/2019/7/20/20700775/fish-pain-love-emotion-animal-cognition-study)
**Vox** (USA) [Online version](https://www.vox.com/future-perfect/2019/7/20/20700775/fish-pain-love-emotion-animal-cognition-study)

[<img src="/img/coverage_embedded_img/media/image58.png"
style="border:1px solid black;width:4in" />](https://www.laliberte.ch/news-agence/detail/comme-les-hommes-les-poissons-ont-aussi-un-attachement-emotionnel/521476)
**La Liberté** (Switzerland) [Online version](https://www.laliberte.ch/news-agence/detail/comme-les-hommes-les-poissons-ont-aussi-un-attachement-emotionnel/521476)

[<img src="/img/coverage_embedded_img/media/image121.png"
style="border:1px solid black;width:4in" />](https://www.dhnet.be/buzz/animaux/ce-poisson-eprouve-un-chagrin-d-amour-lorsqu-il-est-separe-de-sa-moitie-5d0215a4d8ad580bf0501a00)
**DH** (Belgium) [Online version](https://www.dhnet.be/buzz/animaux/ce-poisson-eprouve-un-chagrin-d-amour-lorsqu-il-est-separe-de-sa-moitie-5d0215a4d8ad580bf0501a00)

[<img src="/img/coverage_embedded_img/media/image14.png"
style="border:1px solid black;width:4in" />](https://www.insidescience.org/news/being-stuck-wrong-mate-puts-fish-bad-mood)
**Inside Science** (USA) [Online version](https://www.insidescience.org/news/being-stuck-wrong-mate-puts-fish-bad-mood)

[<img src="/img/coverage_embedded_img/media/image8.png"
style="border:1px solid black;width:4in" />](https://www.newsgroove.co.uk/fish-become-pessimistic-and-lovesick-if-theyre-torn-apart-from-their-true-lover-researchers-find/)
**Newsgroove** (UK) [Online version](https://www.newsgroove.co.uk/fish-become-pessimistic-and-lovesick-if-theyre-torn-apart-from-their-true-lover-researchers-find/)

[<img src="/img/coverage_embedded_img/media/image124.png"
style="border:1px solid black;width:4in" />](https://onties.com/zimbabwe/monogamous-fish-have-shown-that-they-show-pessimistic-biases-when-separated-from-their-colleagues/)
**Onties** (Zimbabwe) [Online version](https://onties.com/zimbabwe/monogamous-fish-have-shown-that-they-show-pessimistic-biases-when-separated-from-their-colleagues/)

[<img src="/img/coverage_embedded_img/media/image19.png"
style="border:1px solid black;width:4in" />](http://en.rfi.fr/contenu/20190612-lovelorn-fish-turn-gloomy-when-separated-study)
**RFI** (France) 
\- [English version](http://en.rfi.fr/contenu/20190612-lovelorn-fish-turn-gloomy-when-separated-study)
\- [Spanish version](http://es.rfi.fr/contenu/20190612-un-pez-tropical-sufre-mal-de-amores-cuando-lo-separan-de-su-pareja)
\- [Farsi version](https://tinyurl.com/y288wmhg)

[<img src="/img/coverage_embedded_img/media/image27.png"
style="border:1px solid black;width:4in" />](http://english.alarabiya.net/en/variety/2019/06/12/Study-says-lovelorn-fish-turn-gloomy-when-separated.html)
**Al Arabiya** (Dubaï) [English version](http://english.alarabiya.net/en/variety/2019/06/12/Study-says-lovelorn-fish-turn-gloomy-when-separated.html)

[<img src="/img/coverage_embedded_img/media/image9.png"
style="border:1px solid black;width:4in" />](https://www.breitbart.com/news/lovelorn-fish-turn-gloomy-when-separated-study/)
**Breitbart** (USA) [Online version](https://www.breitbart.com/news/lovelorn-fish-turn-gloomy-when-separated-study/)

[<img src="/img/coverage_embedded_img/media/image92.png"
style="border:1px solid black;width:4in" />](https://www.zmescience.com/science/fish-romantic-love-partner-2525234/)
**ZME Science** (International, 2019)[Online version](https://www.zmescience.com/science/fish-romantic-love-partner-2525234/)

[<img src="/img/coverage_embedded_img/media/image29.png"
style="border:1px solid black;width:4in" />](https://www.maxisciences.com/animal/amour-les-poissons-aussi-peuvent-avoir-le-coeur-brise_art43351.html)
**Maxisciences** (Belgium, 2019) [Online version](https://www.maxisciences.com/animal/amour-les-poissons-aussi-peuvent-avoir-le-coeur-brise_art43351.html)

[<img src="/img/coverage_embedded_img/media/libertytimes_juin2019s.jpg"
style="border:1px solid black;width:4in" />](/img/coverage_embedded_img/media/libertytimes_juin2019.jpg)
**Liberty Times** (China, 2019) [Printed version](/img/coverage_embedded_img/media/libertytimes_juin2019.jpg)

<img src="/img/coverage_embedded_img/media/image94.png"
style="border:1px solid black;width:4in" />
**Jouvert radio** (Carribea, 2019) 

<img src="/img/coverage_embedded_img/media/image103.png"
style="width:2in" />
**Le Monde** (France, 2019) Passeur de sciences (Pierre Barthélémy)

[<img src="/img/coverage_embedded_img/media/image59.jpg"
style="width:2in"
alt="telegraph-logo" />](https://www.telegraph.co.uk/science/2019/06/11/fish-separated-mate-pine-become-pessimistic/)
**The Telegraph** (UK) [Article](https://www.telegraph.co.uk/science/2019/06/11/fish-separated-mate-pine-become-pessimistic/)

[<img src="/img/coverage_embedded_img/media/image18.png"
style="width:2in"
alt="daily-mail-logo-png-transparent - Ratecard" />](https://www.dailymail.co.uk/sciencetech/article-7130869/Fish-pessimistic-lovesick-theyre-torn-apart-true-lover-researchers-find.html)
**Dailymail** (UK) [Article](https://www.dailymail.co.uk/sciencetech/article-7130869/Fish-pessimistic-lovesick-theyre-torn-apart-true-lover-researchers-find.html)

<img src="/img/coverage_embedded_img/media/image64.png"
style="width:2in" />
**Europe 1** (France) Daily news (Matinale, Nikos Aliagas, David Abiker)



[<img src="/img/coverage_embedded_img/media/image2.jpg"
style="width:1.5in" alt="ABC News" />](https://abcnews.go.com/Technology/female-fish-sad-stick-tank-partner-dont/story?id=63760370)
**ABC news** (USA) 

[<img src="/img/coverage_embedded_img/media/image21.jpg"
style="width:1.5in"
alt="Site Femmeactuelle" />](https://www.femmeactuelle.fr/animaux/news-animaux/ces-poissons-ont-aussi-des-chagrins-damour-2079539)
**Femme actuelle** (France) [Online version](https://www.femmeactuelle.fr/animaux/news-animaux/ces-poissons-ont-aussi-des-chagrins-damour-2079539)

[<img src="/img/coverage_embedded_img/media/image23.png"
style="width:1.5in" />](https://drive.google.com/file/d/1f-8l8nIOqCzM94ALlDXtO4QktkVN0wnh/view?usp=sharing)
**Astrapi** (France, October 2019) [printed version](https://drive.google.com/file/d/1f-8l8nIOqCzM94ALlDXtO4QktkVN0wnh/view?usp=sharing)

[<img src="/img/coverage_embedded_img/media/image126.png"
style="width:2in" alt="La Croix" />](https://drive.google.com/file/d/1Z9kdqRyBP_V8e9pdiD1rRmRFWRCHBZK4/view)
**La Croix** (France, 2019) [printed version](https://drive.google.com/file/d/1Z9kdqRyBP_V8e9pdiD1rRmRFWRCHBZK4/view)

<img src="/img/coverage_embedded_img/media/image17.png"
style="width:2in" />
**Libération** (France) 

[<img src="/img/coverage_embedded_img/media/image38.jpg"
style="width:2in"
alt="Tribune de Genève" />](https://www.tdg.ch/savoirs/sciences/poissons-chagrins-amour/story/15027345)
**Tribune de Genève** (Switzerland) [Online version](https://www.tdg.ch/savoirs/sciences/poissons-chagrins-amour/story/15027345)

[<img src="/img/coverage_embedded_img/media/image113.png"
style="width:2in" />](https://www.lematin.ch/savoirs/sciences/Les-poissons-aussi-ont-des-chagrins-d-amour/story/15027345)
**Le Matin** (Switzerland) [Online version](https://www.lematin.ch/savoirs/sciences/Les-poissons-aussi-ont-des-chagrins-d-amour/story/15027345)


[<img src="/img/coverage_embedded_img/media/image5.png"
style="width:2in" />](https://www.lexpress.fr/actualite/societe/environnement/les-poissons-aussi-ont-des-chagrins-d-amour_2083577.html)
**L’Express** (France) [Online version](https://www.lexpress.fr/actualite/societe/environnement/les-poissons-aussi-ont-des-chagrins-d-amour_2083577.html)


[<img src="/img/coverage_embedded_img/media/image7.png"
style="width:2in" />](https://www.larecherche.fr/biologie-cognition-animale-%C3%A9volution/les-poissons-aussi-souffrent-de-l%E2%80%99absence-de-leur-partenaire)
**La Recherche** (France) [Online version](https://www.larecherche.fr/biologie-cognition-animale-%C3%A9volution/les-poissons-aussi-souffrent-de-l%E2%80%99absence-de-leur-partenaire)


[<img src="/img/coverage_embedded_img/media/image35.png"
style="width:2in" />](https://www.lopinion.fr/blog/billet-michel-schifres/chagrin-d-amour-189865)
**L’Opinion** (France) [Online version](https://www.lopinion.fr/blog/billet-michel-schifres/chagrin-d-amour-189865)

[<img src="/img/coverage_embedded_img/media/image55.png"
style="width:2in" />](http://www.leparisien.fr/sciences/les-poissons-aussi-ont-des-chagrins-d-amour-selon-des-chercheurs-francais-12-06-2019-8091561.php)
**Le Parisien** (France) [Online version](http://www.leparisien.fr/sciences/les-poissons-aussi-ont-des-chagrins-d-amour-selon-des-chercheurs-francais-12-06-2019-8091561.php)

[<img src="/img/coverage_embedded_img/media/image106.jpg"
style="border:1px solid black;width:1.5in"/>](https://www.20minutes.fr/planete/2539355-20190613-petit-poisson-aussi-souffre-chagrins-amour)
**20 minutes** (France) [Online version](https://www.20minutes.fr/planete/2539355-20190613-petit-poisson-aussi-souffre-chagrins-amour)

[<img src="/img/coverage_embedded_img/media/image12.png"
style="width:2in" />](https://www.republicain-lorrain.fr/sante-et-medecine/2019/06/30/c-est-trop-bete-un-chagrin-d-amour)
**Le Républicain Lorrain** (France) [Online version](https://www.republicain-lorrain.fr/sante-et-medecine/2019/06/30/c-est-trop-bete-un-chagrin-d-amour)

[<img src="/img/coverage_embedded_img/media/image97.png"
style="border:1px solid black;width:2in" />](https://www.huffingtonpost.fr/entry/ce-poisson-eprouve-un-chagrin-damour-lorsquil-est-separe-de-sa-moitie_fr_5d00a92ae4b0e7e7816f7976)
**Huffington Post** (France) [Online version](https://www.huffingtonpost.fr/entry/ce-poisson-eprouve-un-chagrin-damour-lorsquil-est-separe-de-sa-moitie_fr_5d00a92ae4b0e7e7816f7976)


[<img src="/img/coverage_embedded_img/media/image90.png"
style="width:2in" />](https://www.quebecscience.qc.ca/sciences/peine-amour-chez-les-poissons/)
**Québec Science** (Canada) [Online version](https://www.quebecscience.qc.ca/sciences/peine-amour-chez-les-poissons/)

[<img src="/img/coverage_embedded_img/media/image77.png"
style="width:2in"
alt="Geo logo Download in HD Quality | Geo, Logos, Logo images" />](https://www.geo.fr/environnement/les-poissons-aussi-ont-des-chagrins-damour-196043)
**Geo** (France) [Online version](https://www.geo.fr/environnement/les-poissons-aussi-ont-des-chagrins-damour-196043)

[<img src="/img/coverage_embedded_img/media/image53.png"
style="width:2in" />](http://www.ecns.cn/hd/2019-06-13/detail-ifzkezvn2341510.shtml)
**ECNS (China)**  [Online version](http://www.ecns.cn/hd/2019-06-13/detail-ifzkezvn2341510.shtml)

[<img src="/img/coverage_embedded_img/media/image33.png"
style="width:2in" />](https://elcomercio.pe/tecnologia/ciencias/pez-sufre-mal-amores-separan-pareja-mexico-colombia-argentina-noticia-644518)
**El Comercio** (Peru) [Online version](https://elcomercio.pe/tecnologia/ciencias/pez-sufre-mal-amores-separan-pareja-mexico-colombia-argentina-noticia-644518)

[<img src="/img/coverage_embedded_img/media/image22.png"
style="width:2in" />](https://www.mercurynews.com/2019/06/12/fish-denied-their-preferred-mate-get-pessimistic-lackadaisical/)
**Mercury News** (USA) [Online version](https://www.mercurynews.com/2019/06/12/fish-denied-their-preferred-mate-get-pessimistic-lackadaisical/)

[<img src="/img/coverage_embedded_img/media/image44.png"
style="width:2in"/>](https://www.sciencesetavenir.fr/nature-environnement/les-poissons-aussi-ont-des-chagrins-d-amour_134413?xtor=RSS-15)
**Science et Avenir** (France) [Online version](https://www.sciencesetavenir.fr/nature-environnement/les-poissons-aussi-ont-des-chagrins-d-amour_134413?xtor=RSS-15)

<img src="/img/coverage_embedded_img/media/image109.jpg"
style="width:2in"
alt="Agence 4uatre on Behance" />
**AFP** (France)

<img src="/img/coverage_embedded_img/media/image3.png"
style="width:2in" />
**France info** (France)
\- [article 1](https://www.francetvinfo.fr/sante/biologie-genetique/les-poissons-peuvent-avoir-des-chagrins-d-amour-demontrent-des-chercheurs-francais_3486067.html) (12/06/2019)
\- [article 2](https://www.francetvinfo.fr/sciences/science-un-poisson-en-proie-a-des-peines-de-coeur_3496069.html) (18/06/2019)

[<img src="/img/coverage_embedded_img/media/image31.png"
style="width:1in" />](https://www.sudouest.fr/2019/06/12/les-poissons-aussi-ont-des-chagrins-d-amour-6199832-6095.php)
**Sud-Ouest** (France) [Online version](https://www.sudouest.fr/2019/06/12/les-poissons-aussi-ont-des-chagrins-d-amour-6199832-6095.php)

[<img src="/img/coverage_embedded_img/media/image4.png"
style="width:2in" />](https://www.larepubliquedespyrenees.fr/2019/06/12/les-poissons-aussi-ont-des-chagrins-d-amour,2567798.php)
**La République des Pyrénées** (France) [Online version](https://www.larepubliquedespyrenees.fr/2019/06/12/les-poissons-aussi-ont-des-chagrins-d-amour,2567798.php)

[<img src="/img/coverage_embedded_img/media/image46.png"
style="width:1in" />](https://www.lapresse.ca/actualites/sciences/201906/11/01-5229773-les-poissons-aussi-ont-des-chagrins-damour.php)
**La Presse** (Canada) [Online version](https://www.lapresse.ca/actualites/sciences/201906/11/01-5229773-les-poissons-aussi-ont-des-chagrins-damour.php)

[<img src="/img/coverage_embedded_img/media/image1.png"
style="width:2in" />](https://www.schwaebische-post.de/politik/liebe-leid-aquarium/1792768/)
**Schwaebische Post** (Germany) [Online version](https://www.schwaebische-post.de/politik/liebe-leid-aquarium/1792768/)

[<img src="/img/coverage_embedded_img/media/image62.png"
style="width:1in" />](https://information.tv5monde.com/info/les-poissons-aussi-ont-des-chagrins-d-amour-305613)
**TV5 Monde** (France) [Online version](https://information.tv5monde.com/info/les-poissons-aussi-ont-des-chagrins-d-amour-305613)

[<img src="/img/coverage_embedded_img/media/image75.png"
style="width:2.5in" />](https://www.laprovence.com/actu/en-direct/5544536/les-poissons-aussi-ont-des-chagrins-damour-quand-ils-sont-separes-de-leur-moitie.html)
**La Provence** (France) [Online version](https://www.laprovence.com/actu/en-direct/5544536/les-poissons-aussi-ont-des-chagrins-damour-quand-ils-sont-separes-de-leur-moitie.html)

[<img src="/img/coverage_embedded_img/media/image67.png"
style="width:1in"/>](https://www.rtl.be/info/magazine/animaux/des-chercheurs-l-ont-prouve-ce-poisson-eprouve-un-chagrin-d-amour-lorsqu-il-est-separe-de-sa-moitie-1132300.aspx)
**RTL** (Belgique) [Online version](https://www.rtl.be/info/magazine/animaux/des-chercheurs-l-ont-prouve-ce-poisson-eprouve-un-chagrin-d-amour-lorsqu-il-est-separe-de-sa-moitie-1132300.aspx)

[<img src="/img/coverage_embedded_img/media/image45.png"
style="width:1in" />](http://www.lessentiel.lu/fr/lifestyle/story/les-poissons-aussi-ont-des-chagrins-d-amour-27939674)
**L’Essentiel** (Luxembourg) [Online version](http://www.lessentiel.lu/fr/lifestyle/story/les-poissons-aussi-ont-des-chagrins-d-amour-27939674)

[<img src="/img/coverage_embedded_img/media/image78.png"
style="border:1px solid black;width:2.5in" />](https://www.cheriefm.fr/reveil-cherie-fm/actualites/le-top-du-reveil-cherie-les-poissons-aussi-peuvent-avoir-des-chagrins-damour-71414969)
**Chérie FM** (France) [Online version](https://www.cheriefm.fr/reveil-cherie-fm/actualites/le-top-du-reveil-cherie-les-poissons-aussi-peuvent-avoir-des-chagrins-damour-71414969)

[<img src="/img/coverage_embedded_img/media/image122.png"
style="width:in" />](https://www.swissinfo.ch/fre/toute-l-actu-en-bref/comme-les-hommes--les-poissons-ont-aussi-un-attachement-%C3%A9motionnel/45024592)
**Swissinfo** (Switzerland) [Online version](https://www.swissinfo.ch/fre/toute-l-actu-en-bref/comme-les-hommes--les-poissons-ont-aussi-un-attachement-%C3%A9motionnel/45024592)

[<img src="/img/coverage_embedded_img/media/image105.png"
style="border:1px solid black;width:1.5in" />](https://www.tvanouvelles.ca/2019/06/11/les-poissons-aussi-ont-des-chagrins-damour)
**TVANouvelles** (Canada) [Online version](https://www.tvanouvelles.ca/2019/06/11/les-poissons-aussi-ont-des-chagrins-damour)

[<img src="/img/coverage_embedded_img/media/image42.png"
style="border:1px solid black;width:2in" />](https://twnews.ch/ch-news/les-poissons-aussi-ont-des-chagrins-d-amour)
**The World news** (Switzerland) [Online version](https://twnews.ch/ch-news/les-poissons-aussi-ont-des-chagrins-d-amour)

[<img src="/img/coverage_embedded_img/media/image116.png"
style="width:2in" />](https://soirmag.lesoir.be/230139/article/2019-06-12/cest-prouve-les-poissons-aussi-connaissent-les-chagrins-damour)
**Le Soir Mag** (Belgium) [Online version](https://soirmag.lesoir.be/230139/article/2019-06-12/cest-prouve-les-poissons-aussi-connaissent-les-chagrins-damour)

[<img src="/img/coverage_embedded_img/media/image54.png"
style="width:2in" />](https://fr.news.yahoo.com/poissons-peuvent-avoir-chagrins-d-092946096.html)
**Yahoo News** (France) [Online version](https://fr.news.yahoo.com/poissons-peuvent-avoir-chagrins-d-092946096.html)

[<img src="/img/coverage_embedded_img/media/image15.png"
style="width:2in" />](https://www.24matins.fr/un-poisson-tropical-capable-de-ressentir-des-chagrins-damour-1059521)
**24matins** (France) [Online version](https://www.24matins.fr/un-poisson-tropical-capable-de-ressentir-des-chagrins-damour-1059521)

[<img src="/img/coverage_embedded_img/media/image81.png"
style="width:2in" />](https://phys.org/news/2019-06-monogamous-fish-pessimistic-bias.html)
**Phys.org** (USA) [Online version](https://phys.org/news/2019-06-monogamous-fish-pessimistic-bias.html)

<img src="/img/coverage_embedded_img/media/image37.png"
style="width:2in" />
**Les infos** (Maroc, 2019) 

[<img src="/img/coverage_embedded_img/media/image26.jpg"
style="border:1px solid black;width:2in"/>](https://drive.google.com/file/d/1d0QWImoTzL3A8XEx5SXNfJ7oClxfMSh5/view)
**L’Expression** (Algeria) [Printed version](https://drive.google.com/file/d/1d0QWImoTzL3A8XEx5SXNfJ7oClxfMSh5/view)

<img src="/img/coverage_embedded_img/media/image34.png"
style="border:1px solid black;width:2in" />
**Ocak** (Turkia, 2019)

<img src="/img/coverage_embedded_img/media/image47.png"
style="border:1px solid black;width:2in" />
**Azinlicka** (Greece, 2019) 

[<img src="/img/coverage_embedded_img/media/image66.png"
style="border:1px solid black;width:2in" />](https://www.goodplanet.info/actualite/2019/06/12/les-poissons-aussi-ont-des-chagrins-damour/)
**Goodplanet** (France, 2019) [Online version](https://www.goodplanet.info/actualite/2019/06/12/les-poissons-aussi-ont-des-chagrins-damour/)

### And also:
**Disneynature** (France)
**Globo** (Brasil)
**Times** (Malta)
**East Bay Time**s (USA)
**Orange actu** (France)
**Aksam** (Turkey)
**Noticiero Científico y Cultural Iberoamericano** (Latin America)
**Xin Wen Guo** (China)
**Gercekci Haber** (Turkey)
**Diken** (Turkey)
**Caucasus** (Armenia)
**ECNS** (China)
**fish-dont-exist** (France)
**TEC Review** (Mexico)
**Milenio** (Mexico)
**El Colombiano** (Colombia)
**Syri** (Albania)
**Canal RCN TV** (Colombia)
**Tribunal du net** (France)
**Baocuaban** (Vietnam)
**Cronica** (Guatemala)
**Viannitak** (Greece)
**The best** (Greece)
**Tiempo de San Juan** (Argentina)
**Sputniknews** (China)
**Visão** (Portugal)
**Ardina** (Portugal)
**Taxydromos** (Greece)
**Kypros** (Chypre)
**Libération Maroc** (Maroc)
**Framtida** (Norway)
**Cup** (Hong-Kong)
**Karapaia** (Japan)
**Uux** (China)
**News Ji-Qi** (China)
**Fédération Française d’Aquariophilie** (France)
**Framtida Junior** (Norway)
**CNRS INEE** (France)
**Daily Views** (Italy)
**Techno Science** (France)
**TV Farandula** (Chile)
**Aeiou** (Portugal)
**La femme qui marche** (France)
**Animal Scene** (Philippines)
**Visão** (Brasil)
**Earth** (USA)
**Bangkok Post** (Thailand)
**Jakarta Post** (Indonesia)
**Blitz** (Bulgaria)
→ For a more complete review see the [media coverage web page](https://docs.google.com/document/d/e/2PACX-1vS_ejHoQYyG7v07zTUcx7Z5GSgO1ZV7b30-yo8IMlJqsoZkGVHG0lcNshZok0_Gjw3q0-6r-DRXjcnH/pub)
[<img src="/img/coverage_embedded_img/media/image56.png"
style="border:1px solid black;width:4in" />](https://docs.google.com/document/d/e/2PACX-1vS_ejHoQYyG7v07zTUcx7Z5GSgO1ZV7b30-yo8IMlJqsoZkGVHG0lcNshZok0_Gjw3q0-6r-DRXjcnH/pub)

[<img src="/img/coverage_embedded_img/media/image65.jpg"
style="border:1px solid black;width:4in" />](http://www.bbc.com/future/story/20181011-are-relationships-better-if-partners-are-more-similar)
**BBC** (October 2018) [online version](http://www.bbc.com/future/story/20181011-are-relationships-better-if-partners-are-more-similar)

[<img src="/img/coverage_embedded_img/media/image13.jpg"
style="border:1px solid black;width:4in" />](/img/coverage_embedded_img/media/especes_septembre_2018.pdf)
**Espèces** (France, September 2018) [pdf version](/img/coverage_embedded_img/media/especes_septembre_2018.pdf)

[<img src="/img/coverage_embedded_img/media/image50.jpg"
style="border:1px solid black;width:4in" />](http://www.letemps.ch/sciences/2016/03/04/s-assemble-finit-se-ressembler-un-temps)
**Le Temps** (Switzerland, 2016) [online version](http://www.letemps.ch/sciences/2016/03/04/s-assemble-finit-se-ressembler-un-temps)

[<img src="/img/coverage_embedded_img/media/image125.jpg"
style="border:1px solid black;width:4in" />](/img/coverage_embedded_img/media/BerlinerZeitung.pdf)
**Berliner Zeitung** (Germany, 2016) [printed version](/img/coverage_embedded_img/media/BerlinerZeitung.pdf)

[<img src="/img/coverage_embedded_img/media/image127.jpg"
style="border:1px solid black;width:4in" />](/img/coverage_embedded_img/media/figaro_science_2016_03_08.jpg)
**Le Figaro** (France, 2016) [printed version](/img/coverage_embedded_img/media/figaro_science_2016_03_08.jpg)

[<img src="/img/coverage_embedded_img/media/image73.png"
style="border:1px solid black;width:4in" />](http://www.stuttgarter-zeitung.de/inhalt.verhalten-das-geheimrezept-der-liebe.c9028fd7-ca22-4795-9c67-260b31850eca.html)
**Stuttgarter Zeitung** (Germany, 2016) [online version](http://www.stuttgarter-zeitung.de/inhalt.verhalten-das-geheimrezept-der-liebe.c9028fd7-ca22-4795-9c67-260b31850eca.html)

[<img src="/img/coverage_embedded_img/media/image70.png"
style="border:1px solid black;width:4in" />](http://www.badische-zeitung.de/liebe-familie/fuer-dich-werd-ich-zum-kaempfer--135194946.html)
**Badische Zeitung** (Germany, 2016) [online version](http://www.badische-zeitung.de/liebe-familie/fuer-dich-werd-ich-zum-kaempfer--135194946.html)

[<img src="/img/coverage_embedded_img/media/image71.png"
style="border:1px solid black;width:4in" />](/img/coverage_embedded_img/media/Wiener_Zeitung_2017.pdf)
**Wiener Zeitung** (Austria, 2016) [Printed version](/img/coverage_embedded_img/media/Wiener_Zeitung_2017.pdf)

[<img src="/img/coverage_embedded_img/media/image118.png"
style="border:1px solid black;width:4in" />](/img/coverage_embedded_img/media/Zentralschweiz_Sonntag_2017.pdf)
**Zentralschweiz** (Switzerland, 2016) [Printed version](/img/coverage_embedded_img/media/Zentralschweiz_Sonntag_2017.pdf)

<img src="/img/coverage_embedded_img/media/image52.png"
style="border:1px solid black;width:2in" alt="Pour la Science" />
**Pour la Science** (France) [online version](http://library.madeinpresse.fr/samples/MPjs9ew5qf6_-f)

<img src="/img/coverage_embedded_img/media/image63.png"
style="border:1px solid black;width:2in"/>
**Le Bien Public** (France) [online version](http://c.bienpublic.com/edition-dijon-ville/2016/03/13/comment-les-animaux-choisissent-leurs-partenaires-sexuels)
and **Le Journal de Saône-et-Loire** (France)
[online version](http://www.lejsl.com/saone-et-loire/2016/03/13/deux-ans-a-etudier-le-choix-des-partenaires-sexuels-des-animaux) and [printed version](/img/coverage_embedded_img/media/saoneloire.jpg)
and **Vosgesmatin** (France) [online version](http://www.vosgesmatin.fr/science-et-technologie/2016/07/01/1259)

<img src="/img/coverage_embedded_img/media/image129.jpg"
style="width:1in"/>
**Ca m'intéresse** (France, May 2016) 

<img src="/img/coverage_embedded_img/media/image102.png"
style="border:1px solid black;width:1in"
alt="Afficher l&#39;image d&#39;origine" />
**Science & Vie** (France, june 2016)

[<img src="/img/coverage_embedded_img/media/image32.png"
style="border:1px solid black;width:4in" />](http://www.sudouest.fr/2016/03/16/quel-est-le-secret-des-couples-qui-durent-les-exemples-chez-les-animaux-2302941-706.php)
**Sud Ouest** (France, 2016) [online version](http://www.sudouest.fr/2016/03/16/quel-est-le-secret-des-couples-qui-durent-les-exemples-chez-les-animaux-2302941-706.php)

<img src="/img/coverage_embedded_img/media/image84.png"
style="border:1px solid black;width:4in" />
**Espèces** (France)

<img src="/img/coverage_embedded_img/media/image112.jpg"
style="border:1px solid black;width:4in" />
**Arte** ([Germany]) [Online version](https://artefuturede.tumblr.com/post/142224635870/in-beziehungsdingen-ist-der-mittelamerikanische)

<img src="/img/coverage_embedded_img/media/image20.png"
style="width:1.5in" alt="France Inter logo" />
**France Inter** (French public radio station, 2016), “C’est tout naturel” [webpage](http://www.franceinter.fr/emission-cest-tout-naturel-revelations-sur-la-vie-de-couple-des-poissons)

<img src="/img/coverage_embedded_img/media/image30.png"
style="width:1.5in" />
**France Bleu Bourgogne** (French public radio station, 2016), "[C'est bon à savoir](https://www.francebleu.fr/emissions/c-est-bon-savoir/besancon/une-franc-comtoise-etudie-la-personnalite-des-poissons-pour-leur-reproduction)"

<img src="/img/coverage_embedded_img/media/image61.jpg"
style="border:1px solid black;width:4in" />
**Radio Deutschlandfunk** ([Germany](https://en.wikipedia.org/wiki/Germany), public radio station, 2016)
[webpage with the mp3 of the interview (in German)](http://www.deutschlandfunk.de/monogame-fische-die-harmonie-kommt-spaeter.676.de.html?dram:article_id=347689)

<img src="/img/coverage_embedded_img/media/image120.jpg"
style="border:1px solid black;width:4in" />
**France Télévision** (France, 2016) [webpage](http://www.francetvinfo.fr/animaux/pour-se-reproduire-les-couples-mal-assortis-finissent-par-se-ressembler_1350097.html)

[<img src="/img/coverage_embedded_img/media/image40.jpg"
style="border:1px solid black;width:4in" />](http://radio24.ua/news/showSingleNews.do?objectId=47132)
**Radio24** (Ukraine, 2016) [webpage](http://radio24.ua/news/showSingleNews.do?objectId=47132)

<img src="/img/coverage_embedded_img/media/image39.jpg"
style="border:1px solid black;width:4in" />
**Pravda TV** (Russia, 2016) [webpage](http://www.pravda-tv.ru/2016/03/05/212943/nazvan-luchshij-sposob-uluchsheniya-otnoshenij-v-pare)

[<img src="/img/coverage_embedded_img/media/image41.png"
style="border:1px solid black;width:4in" />](http://www.economist.com/node/13097814)
**The Economist** (United Kingdom, 2009) [webpage](http://www.economist.com/node/13097814)

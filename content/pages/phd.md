---
title: "Thèse de doctorat"
date: 2022-11-28T14:02:19+01:00
draft: false
languageCode: fr
---

Doctorat en Écologie de l'université Paris VI Pierre-et-Marie-Curie (maintenant Sorbonne Université), effectuée sous la direction de Minh-Hà Pham-Delègue (Laboratoire de Neurobiologie Comparée des Invertébrés, Bures-sur-Yvette) et d'Odile Pons (Laboratoire de Biométrie, Jouy-en-Josas) : "Le comportement de butinage collectif chez l'abeille: étude théorique et expérimentale". Soutenue le **22 décembre 2003**, devant le jury constitué de :
- Patrick Porcheron, Université Pierre-et-Marie-Curie Paris VI (président),
- Marc Jarry, Université de Pau (rapporteur),
- Guy Theraulaz, CNRS Toulouse (rapporteur),
- Jean-Sébastien Pierre, Université de Rennes 1
- Odile Pons, INRA Jouy-en-Josas
- Minh-Hà Pham-Delègue, INRA Bures-sur-Yvette 

**Résumé de la thèse** : La collecte de nectar chez l'abeille met en jeu des processus collectifs reposant sur une communication vibratoire intense entre les membres de la colonie qui agissent de façon coordonnée. A partir des données recueillies lors d'expérimentations conduites au laboratoire, j'ai étudié les transitions entre les différents comportements impliqués dans le butinage (modèles à risques proportionnels). J'ai en outre développé plusieurs modèles mathématiques (systèmes dynamiques non linéaires) du comportement de butinage. Ce travail m'a conduit à reconsidérer le langage de l'abeille dans un contexte d'écologie comportementale et à proposer un scénario de l'évolution de la communication de la distance chez les apoïdes. En outre, les protocoles expérimentaux et les outils d'analyse statistique développés dans cette thèse ont été utilisés pour évaluer l'impact de produits phytosanitaires sur le comportement de butinage collectif des abeilles.

**Manuscrit de thèse** : [[téléchargez au format pdf, 5 Mo] ](http://www.fxdm.org/download/fxdm(2003)these.pdf)
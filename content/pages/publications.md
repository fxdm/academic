---
title: "Publications"
date: 2022-11-28T14:02:19+01:00
draft: false
---

#### Peer reviewed journals

Pagnon T., Etchart L,. Teixeira M., Dechaume-Moncharmont F.-X., Hallgrimsson G.T., Hansen J., Lang J., Moreau J., Reneerkens J., Schmidt N.M. Soloviev M., Ten Horn J., Tomkovich P., Wood A.G., Yannic G., Bollache L., Gilg O. (2024) Using a common morphometric-based method to sex a migratory bird along its entire flyway despite geographical and temporal variations in body size and sexual size dimorphism. **Journal of Ornithology** [\[pdf\]](http://www.fxdm.org/download/pagnon_24_using.pdf) [\[doi\]](http://dx.doi.org/10.1007/s10336-024-02178-9)

[<img src="/img/coverage_embedded_img/media/etchart_24_extended_COVER.gif"
style="border:0px solid black; height:1.5in"/>](http://www.fxdm.org/download/etchart_24_extended.pdf) 
Etchart L., Lecomte N., Dechaume-Moncharmont F.-X., Moreau J., Lang J., Pagnon T., Sittler B., Teixeira M., Bollache L., Gilg O. (2024) Extended incubation recesses in sanderlings are impacted by temperature and body condition. **Proceedings of the Royal Society B : Biological Sciences** 291:20232264 [\[pdf\]](http://www.fxdm.org/download/etchart_24_extended.pdf) [\[doi\]](http://dx.doi.org/10.1098/rspb.2023.2264) 

Charalabidis A., Derocles S.A.P., Mosquera-Muñoz D.M., Petit S., Dechaume-Moncharmont F.-X., Bohan D.A. (2023) Sex-specific foraging response to interspecific interactions in carabid beetles. **Biological Control** 185:105302  [\[pdf\]](http://www.fxdm.org/download/charalabidis_23_sexspecific.pdf) [\[doi\]](http://dx.doi.org/10.1016/j.biocontrol.2023.105302)

Fernandez-Declerck M., Rojas E., Prosnier L., Teulier L., Dechaume-Moncharmont F.-X., Médoc V. (2023) Adding insult to injury: anthropogenic noise intensifies predation risk by an invasive freshwater fish species. **Biological Invasions** 25:2775-2785 [\[pdf\]](http://www.fxdm.org/download/fernandez-declerck_23_adding.pdf) [\[doi\]](http://dx.doi.org/10.1007/s10530-023-03072-w) [\[data\]](https://doi.org/10.5281/zenodo.6563527)

Depeux C., Branger A., Moulignier T., Moreau J., Lemaître J.-F., Dechaume-Moncharmont F.-X., Laverre T., Paulhac H., Gaillard J.-M., Beltran-Bech S. (2023) Deleterious effects of thermal and water stresses on life history and physiology: a case study on woodlouse. **Peer Community Journal** 3:e7 [\[pdf\]](http://www.fxdm.org/download/depeux_23_deleterious.pdf) [\[doi\]](http://dx.doi.org/10.24072/pcjournal.228) [\[data\]](https://doi.org/10.5281/zenodo.7496836)

Lartigue S., Yalaoui M., Belliard J., Caravel C., Jeandroz L., Groussier G., Calcagno V., Louâpre P., Dechaume-Moncharmont F.-X., Malausa T., Moreau J. (2022) Consistent variations in personality traits and their potential for genetic improvement of biocontrol agents: _Trichogramma evanescens_ as a case study. **Evolutionary Applications** 15:1565-1579 [\[doi\]](http://dx.doi.org/10.1111/eva.13329) [\[pdf\]](http://www.fxdm.org/download/lartigue_22_consistent.pdf) [\[data\]](http://doi.org/10.5281/zenodo.4058218)

Meyer N., Bollache L., Galipaud M., Moreau J., Dechaume-Moncharmont F.-X., Afonso E., Angerbjörn A., Bêty J., Brown G., Ehrich D., Gilg V., Giroux M.-A., Hansen J., Lanctot R.B., Lang J., Latty C., Lecomte N., McKinnon L., Kennedy L., Reneerkens J., Saalfeld S.T., Sabard B., Schmidt N.M., Sittler B., Smith P., Sokolov A., Sokolov V., Sokolova N., van Bemmelen R., Varpe Ø., Gilg O. (2021) Behavioural responses of breeding arctic sandpipers to ground-surface temperature and primary productivity. **Science of Total Environment** 755:142485 [\[doi\]](http://dx.doi.org/10.1016/j.scitotenv.2020.142485) [\[pdf\]](http://www.fxdm.org/download/meyer_21_behavioural_4422425.pdf)

Lartigue S., Yalaoui M., Belliard J., Caravel C., Jeandroz L., Groussier G., Calcagno V., Louâpre P., Dechaume-Moncharmont F.-X., Malausa T., Moreau J. (2020) Consistent variations in personality traits and their potential for genetic improvement of biocontrol agents: _Trichogramma evanescens_ as a case study. **PCI Ecology** [\[doi\]](http://dx.doi.org/10.1101/2020.08.21.257881) [\[pdf\]](http://www.fxdm.org/download/lartigue_20_consistent.pdf) [\[data\]](http://doi.org/10.5281/zenodo.4058218)  --> now published as Lartigue et al. (2022)

Chevalier L., Labonne J., Galipaud M., Dechaume-Moncharmont F.-X. (2020) Fluctuating dynamics of mate availability promote the evolution of flexible choosiness in both sexes. **American Naturalist** 196:730-742 [\[doi\]](https://doi.org/10.1086/711417) [\[pdf\]](http://www.fxdm.org/download/chevalier_20_fluctuating.pdf) [\[source code\]](https://gitlab.com/fxdm/flexible-mate-choice) [\[shinyapps\]](https://louise64.shinyapps.io/choosiness/)

Boucheker A., Nedjah R., Prodon R., Gillingham M., Dechaume-Moncharmont F.-X., Béchet A., Samraoui B. (2020) Cohort effect on discriminant rate: the case of greater flamingo (_Phœnicopterus roseus_) chicks sexed with morphological characters. **Web Ecology** 20:153–159 [\[doi\]](https://doi.org/10.5194/we-20-153-2020) [\[pdf\]](http://www.fxdm.org/download/boucheker_20_cohort_356438489.pdf)

Depeux C., Lemaître J.-F., Moreau J., Dechaume-Moncharmont F.-X., Laverre T., Pauhlac H., Gaillard J.-M., Beltran-Bech S. (2020) Reproductive senescence and parental effect in an indeterminate grower. Journal of Evolutionary Biology. **Journal of Evolutionary Biology** 33:1256-1264 [\[doi\]](http://dx.doi.org/10.1111/jeb.13667) [\[pdf\]](http://www.fxdm.org/download/depeux_20_reproductive_25445.pdf)

Meyer N., Bollache L., Dechaume-Moncharmont F.-X., Moreau J., Afonso1 E., Angerbjörn A., Bêty J., Ehrich D., Gilg V., Giroux M.-A., Hansen J., Lanctot R. B., Lang J., Lecomte N., McKinnon L., Reneerkens J., Saalfeld S. T., Sabard B., Schmidt N. M., Sittler B., Smith P., Sokolov A., Sokolov V., Sokolova N., van Bemmelen R., Gilg O. (2020) Nest attentiveness drives nest predation in arctic sandpipers. **Oikos** 129:1481-1492 [\[doi\]](https://doi.org/10.1111/oik.07311) [\[pdf\]](http://www.fxdm.org/download/meyer_20_nest_25487.pdf)[  
](https://doi.org/10.1111/oik.07311)

Fayard M., Dechaume-Moncharmont F.-X., Wattier R., Perrot-Minnot M.-J. (2020) Magnitude and direction of parasite-induced phenotypic alterations: a meta-analysis in Acanthocephalans. **Biological Reviews** 95:1233-1251 [\[doi\]](http://dx.doi.org/10.1111/brv.12606) [\[pdf\]](http://www.fxdm.org/download/fayard_20_magnitude_35846.pdf) [\[data and source code\]](http://www.fxdm.org/download/fayard_20_magnitude_DATA.zip)

Depeux C., Samba-Louaka A., Becking T., Braquart-Varnier C., Moreau J., Lemaître J.-F., Laverre T., Paulhac H., Dechaume-Moncharmont F.-X., Gaillard J.-M., Beltran-Bech S. (2020) The crustacean _Armadillidium vulgare_ (Latreille, 1804) (Isopoda: Oniscoidea), a new promising model for the study of cellular senescence. **Journal of Crustacean Biology** 40:194–199 [\[doi\]](http://dx.doi.org/10.1093/jcbiol/ruaa004) [\[pdf\]](http://www.fxdm.org/download/depeux_20_crustacean_438463.pdf)

Laubu C., Louâpre P., Dechaume-Moncharmont F.-X. (2019) Pair-bonding influences affective state in a monogamous fish species. **Proceedings of the Royal Society B: Biological Sciences** 286:20190760 [\[pdf\]](http://www.fxdm.org/download/laubu_19_pairbonding_24343246.pdf) + [\[dataset\]](https://datadryad.org/resource/doi:10.5061/dryad.m7v5t98) + [\[media coverage\]](https://docs.google.com/document/d/e/2PACX-1vS_ejHoQYyG7v07zTUcx7Z5GSgO1ZV7b30-yo8IMlJqsoZkGVHG0lcNshZok0_Gjw3q0-6r-DRXjcnH/pub)

Charalabidis A., Dechaume-Moncharmont F.-X., Carbonne B., Bohan D., Petit S. (2019) Diversity of foraging strategies and responses to predator interference in seed-eating carabid beetles. **Basic and Applied Ecology** 36:13-24 [\[pdf\]](http://www.fxdm.org/download/charalabidis_19_diversity_bae.pdf)

Couchoux C., Dechaume-Moncharmont F.-X., Rigaud T., Bollache L. (2018) Male _Gammarus roeseli_ provide smaller ejaculates to females infected with vertically transmitted microsporidian parasites. **Animal Behaviour** 137:179-185 [\[pdf\]](http://www.fxdm.org/download/couchoux_18_male.pdf)

Charalabidis A., Dechaume-Moncharmont F.-X., Petit S., Bohan D. (2017) Risk of predation makes foragers less choosy about their food. **PLoS ONE** 12(11):e0187167 [\[open access\]](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0187167) + [\[dataset\]](https://doi.org/10.1371/journal.pone.0187167.s004)

Galipaud M., Gillingham M.A.F., Dechaume-Moncharmont F.-X. (2017) A farewell to the sum of Akaike weights: the benefits of alternative metrics for variable importance estimations in model selection. **Methods in Ecology and Evolution** 8:1668-1678 [\[pdf\]](http://www.fxdm.org/download/galipaud_17_farewell.pdf) + [\[R source code\]](https://besjournals.onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2F2041-210X.12835&attachmentId=205618898)

Iltis C., Dechaume-Moncharmont F.-X., Galipaud M., Moreau J., Bollache L., Louâpre P. (2017) The curse of being single: male and female *Gammarus pulex* both energetically benefit from precopulatory mate guarding. **Animal Behaviour** 130:67-72 [\[pdf\]](http://www.fxdm.org/download/iltis_17_curse.pdf)

Monceau K., Dechaume-Moncharmont F.-X., Moreau J., Lucas C., Capoduro R., Motreuil S., Moret Y. (2017) Personality, immune response and reproductive success : an appraisal of the pace-of-life syndrome hypothesis. **Journal of Animal Ecology** 86:932-942 [\[pdf\]](http://www.fxdm.org/download/monceau_17_personality_1111749.pdf)

Laubu C., Schweitzer C., Motreuil S., Louâpre P., Dechaume-Moncharmont F.-X. (2017) Mate choice based on behavioural type : Do convict cichlids prefer similar partners? **Animal Behaviour** 126:281-291 [\[pdf\]](http://www.fxdm.org/download/laubu_17_mate_1105548.pdf)

Schweitzer C., Melot G., Laubu C., Teixeira M., Motreuil S., Dechaume-Moncharmont F.-X. (2017) Hormonal and fitness consequences of behavioral assortative mating in the convict cichlid (_Amatitlania siquia_). **General and Comparative Endocrinology** 240:15-161 [\[pdf\]](http://www.fxdm.org/download/schweitzer_17_hormonal.pdf)

Monceau K., Moreau J., Richet J., Motreuil S., Moret Y., Dechaume-Moncharmont F.-X. (2017) Larval personality does not predict adult personality in a holometabolous insect. **Biological Journal of The Linnean Society**, 120(4):869-878 [\[pdf\]](http://www.fxdm.org/download/monceau_17_larval.pdf) [\[dataset\]](http://www.fxdm.org/download/monceau_17_larval_dataset.zip)

Laubu C., Dechaume-Moncharmont F.-X., Motreuil S., Schweitzer C. (2016) Mismatched partners that achieve postpairing behavioral similarity improve their reproductive success. **Science Advances**, 2(3):e1501013 [\[open access\]](http://advances.sciencemag.org/content/2/3/e1501013.full) [\[media coverage\]](https://docs.google.com/document/d/1pJZEj0f4EeZroMEF0y_ZwSQVcFYB6TNxhkcVp9srzbY/pub)

Dechaume-Moncharmont F.-X., Brom T., Cézilly F. (2016) Opportunity costs resulting from scramble competition within the choosy sex severely impair mate choosiness. **Animal Behaviour** 114:249-260 [\[pdf\]](http://www.fxdm.org/download/dechaume-moncharmont_16_opportunity_1059352.pdf)

Bitome Essono P.-Y, Dechaume-Moncharmont F.-X., Mavoungou J., Obiang Mba R., Duvallet G., Bretagnolle F. (2015) Distribution and abundance of hematophagous flies (Glossinidae, Stomoxys and Tabanidae) in two national parks of Gabon. **Parasite** 22:23 [\[pdf\]](http://www.fxdm.org/download/bitome_essono_15_distribution_1026449.pdf)

Schweitzer C., Motreuil S., Dechaume-Moncharmont F.-X. (2015) Coloration reflects behavioural types in the convict cichlid (_Amatitlania siquia_). **Animal Behaviour** 105:201-209 [\[pdf\]](http://www.fxdm.org/download/schweitzer_15_coloration_1019551.pdf)

Mazué G.P.F., Dechaume-Moncharmont F.-X., Godin J.-G.J (2015) Boldness-exploration behavioral syndrome: inter-family variability and repeatability of personality traits in the young of the convict cichlid (_Amatitlania siquia_). **Behavioral Ecology** 26:900-908 [\[pdf\]](http://www.fxdm.org/download/mazue_15_boldnessexploration_1018027.pdf)

Galipaud M., Bollache L., Wattier R., Dubreuil C., Dechaume-Moncharmont F.-X., Lagrue C. (2015) Overestimation of the strength of size-assortative pairing in taxa with cryptic diversity: a case of Simpson's paradox. **Animal Behaviour** 102:217-221 [\[pdf\]](http://www.fxdm.org/download/galipaud_15_overestimation_1006692.pdf)

Galipaud M., Bollache L., Oughadou A., Dechaume-Moncharmont F.-X. (2015) Males do not always switch females when presented to a better reproductive option. **Behavioral Ecology** 26:359–366 [\[pdf\]](http://www.fxdm.org/download/Galipaud_2015_males_do_not_always_switch_females.pdf)

Galipaud M., Gillingham M.A.F., David M., Dechaume-Moncharmont F.-X. (2014) Ecologists overestimate the importance of predictor variables in model averaging: a plea for cautious interpretations. **Methods in Ecology and Evolution** 5:983–991 [\[pdf\]](http://www.fxdm.org/download/galipaud_2014_MEE.pdf) \+ [\[R source code\]](http://www.fxdm.org/download/galipaud_2014_MEE_R_code.zip)

Quinard A., Dechaume-Moncharmont F.-X., Cézilly F. (2014) Pairing patterns in relation to body size, genetic similarity and multilocus heterozygosity in a tropical monogamous bird species. **Behavioral Ecology and Sociobiology** 68:1723-1731 [\[pdf\]](http://www.fxdm.org/download/quinard_14_pairing_986935.pdf)

Dechaume-Moncharmont F.-X., Freychet M., Motreuil S., Cézilly F. (2013). Female mate choice in convict cichlids is transitive and consistent with a self-referent directional preference. **Frontiers in Zoology** 10:69 [\[open access\]](http://www.frontiersinzoology.com/content/10/1/69) + original dataset [\[zip\]](/download/dechaume_2013_transitvity_dataset.zip)

Galipaud M., Bollache L., Dechaume-Moncharmont F.-X. (2013) Assortative mating by size without a size-based preference: the female-sooner norm as a mate guarding criterion. **Animal Behaviour** 85:35-41 [\[pdf\]](http://www.fxdm.org/download/galipaud_13_assortative_864896.pdf)

Monceau K., Wattier R., Dechaume-Moncharmont F.-X., Dubreuil C., Cézilly F. (2013) Heterozygoty-fitness correlations in adult and juvenile zenaida dove, _Zenaida aurita_. **Journal of Heredity** 104:47-56 [\[pdf\]](http://www.fxdm.org/download/monceau_13_heterozygosityfitness_864900.pdf)

David M., Auclair Y., Dechaume-Moncharmont F.-X., Cézilly F. (2012) Handling stress does not reflect personality in female zebra finches (_Taeniopygia guttata_). **Journal of Comparative Psychology** 126:10-14 [\[pdf\]](http://www.fxdm.org/download/david_12_handling_759097.pdf)

Dechaume-Moncharmont F.-X., Cornuau J.H.,  Keddar I., Ihle M., Motreuil S., Cézilly F. (2011) Rapid assessment of female preference for male size predicts subsequent choice of spawning partner in a socially monogamous cichlid fish. **C. R. Biol.** 334:906-910 [\[pdf\]](http://www.fxdm.org/download/dechaume-moncharmont_11_rapid_724574.pdf)

Dechaume-Moncharmont F.-X., Monceau K., Cézilly F. (2011) Sexing birds using discriminant function analysis: A critical appraisal. **The Auk** 128:78-86 [\[pdf\]](http://www.fxdm.org/download/dechaume_2011_auk.pdf) + [[Dataset and R code]](https://gitlab.com/fxdm/dfa-sexing) (cross-validated by [Vines et al. 2014](http://dx.doi.org/10.1016/j.cub.2013.11.014), and [Andrew et al. 2015](http://dx.doi.org/10.7717/peerj.1137) )

Galipaud M., Dechaume-Moncharmont F.-X., Oughadou A, Bollache L. (2011) Does foreplay matter? _Gammarus pulex_ females may benefit from long-lasting precopulatory mate guarding. **Biology Letters** 7:333-335 [\[pdf\]](http://www.fxdm.org/download/galipaud(2011)biol.letters.pdf)

Monceau K., Wattier R., Dechaume-Moncharmont F.-X., Motreuil S., Cézilly F. (2011) Territoriality vs. flocking in the zenaida dove, _Zenaida aurita:_ resource polymorphism revisited using morphological and genetic analyses. **The Auk** 128:15-25 [\[pdf\]](http://www.fxdm.org/download/monceau_2011_auk.pdf)

Franceschi N., Cornet S., Bollache L., Dechaume-Moncharmont F.-X., Bauer A., Motreuil S., Rigaud T. (2010) Variation between populations and local adaptation in acanthocephalan-induced manipulation. **Evolution** 64:2417–2430 [\[pdf\]](http://www.fxdm.org/download/franceschi_10_variation_548317.pdf)

Franks N.R., Dechaume-Moncharmont F.-X., Hanmore E., Reynolds J.K. (2009) Speed versus accuracy in decision-making ants: expediting politics and policy implementation. **Philosophical Transactions of the Royal Society B: Biological Sciences** 364:845–852 [\[pdf\]](http://www.fxdm.org/download/franks_09_speed_548316.pdf) [\[media coverage\]](http://www.economist.com/node/13097814)

Groß R., Houston A.I., Collins E.J., McNamara J.M., Dechaume-Moncharmont F.-X., Franks N.R. (2008) Simple learning rules to cope with changing environments. **Journal of the Royal Society Interface** 5:1193-1202 [\[pdf\]](http://www.fxdm.org/download/gross_08_simple_551311.pdf)

Planqué R., Dechaume-Moncharmont F.-X., Franks N.R., Kovacs T., Marshall J.A.R. (2007) Why do house-hunting ants recruit in both directions? **Naturwissenschaften**, 94:911-918 [\[pdf\]](http://www.fxdm.org/download/planque_07_why_548257.pdf)

Dornhaus A., Collins E.J., Dechaume-Moncharmont F.-X., Houston A.I, Franks N.R., McNamara J.M. (2006) Paying for information: partial loads in central place foragers. **Behavioural Ecology and Sociobiology** 61:151–161 [\[pdf\]](http://www.fxdm.org/download/dornhaus_06_paying_548315.pdf)

Lenoir J.-C., Laloi D., Dechaume-Moncharmont F.-X., Solignac M. & Pham-Delègue M.-H. (2006) Intra-colonial variation of the sting extension response in the honey bee _Apis mellifera_. **Insectes Sociaux** 53:80-85 [\[pdf\]](http://www.fxdm.org/download/lenoir_06_intracolonial_548321.pdf)

Dechaume-Moncharmont F.-X., Dornhaus A., Houston A.I., McNamara, J.M., Collins E.J., Franks N.R. (2005) The hidden cost of information in collective foraging. **Proceedings of the Royal Society B: Biological Sciences** 272:1689-1695 [\[pdf\]](http://www.fxdm.org/download/dechaume-moncharmont_05_hidden_548319.pdf)

Desneux N., Fauvergue X, Dechaume-Moncharmont F.-X., Kerhoas L., Ballanger Y. & Kaiser L. (2005) The parasitoid _Diaeretiella rapae_ can limit populations of the aphid *Myzus persicae* following application of deltamethrin in oilseed rape. **Journal of Economic Entomology** 98:9-17 [\[pdf\]](http://www.fxdm.org/download/desneux_05_emphdiaeretiella_548320.pdf)

Dechaume-Moncharmont F.-X., Azzouz H. , Pons O. Pham-Delègue M.-H. (2005) Soybean proteinase inhibitor and the foraging strategy of free flying honeybees. **Apidologie** 36:421-430 [\[pdf\]](http://www.fxdm.org/download/dechaume-moncharmont_05_soybean_548322.pdf)

Dechaume-Moncharmont F.-X., Decourtye A., Hennequet, C, Pons O. & Pham-Delègue M.-H. (2003) Statistical analysis of the Honeybee survival after chronic exposure to insecticides, **Environmental Toxicology and Chemistry** 22:3088-3094 [\[pdf\]](http://www.fxdm.org/download/dechaume_moncharmont_03_statistical_548318.pdf)

#### Book chapters

Dechaume-Moncharmont F.-X. (2019) Approche évolutionniste du comportement animal, in: _Ethologie animale_ (A.-S. Darmaillacq & F. levy), 2e édition, de Boeck, Louvain-la-Neue [\[web\]](https://www.deboecksuperieur.com/ouvrage/9782807320376-ethologie-animale) [\[amazon\]](https://www.amazon.fr/Ethologie-animale-approche-biologique-comportement/dp/2807320376)

Dechaume-Moncharmont F.-X. (2018) Evolutionarily stable strategies, in: _Encyclopedia of Animal Cognition and Behavior_ (J. Vonk & T. Shackelford Eds), Springer Verlag, Berlin [\[pdf\]](http://www.fxdm.org/download/dechaume-moncharmont_18_evolutionarily_1130498.pdf)

Franks N., Dornhaus A., Marshall J., Dechaume-Moncharmont F.-X. (2009) The dawn of a golden age in mathematical sociobiology, in: _Organization of insect societies: from genome to sociocomplexity_ (J. Fewell & J. Gadau Eds), Harvard University Press, Harvard, USA, p. 437-459 [\[pdf\]](http://www.fxdm.org/download/franks_09_dawn.pdf)

#### Reviews

Dechaume-Moncharmont F.-X. (2020) Extreme weight loss: when accelerometer could reveal reproductive investment in a semelparous fish species. **Peer Community in Ecology** 100060 \[[doi](http://dx.doi.org/10.24072/pci.ecology.100060)\] \[[pdf](http://www.fxdm.org/download/dechaume-moncharmont_20_extreme_242842.pdf)\]

Dechaume-Moncharmont F.-X. (2020) Touchy matter: the delicate balance between Morgan's canon and open-minded description of advanced cognitive skills in the animal. **Peer Community In Ecology**, 100042 [\[doi\]](https://doi.org/10.24072/pci.ecology.100042)

Dechaume-Moncharmont F.-X. (2018) Optimal foraging in a changing world: old questions, new perspectives. **Peer Community In Ecology**, 100005 [\[doi\]](http://dx.doi.org/10.24072/pci.ecology.100005) [\[pdf\]](http://www.fxdm.org/download/dechaume-moncharmont_18_optimal_1145193.pdf)

#### Popular science

Laubu C. et Dechaume-Moncharmont F.-X. (2016) Le choix du partenaire : une force évolutive sous-estimée. _La nature est-elle bien faite ?_ (Cécile Breton Ed.), Le Cavalier Bleu, Paris, p. 49-56 [\[editeur\]](http://www.lecavalierbleu.com/livre/la-nature-est-elle-bien-faite/) [\[amazon\]](https://www.amazon.fr/nature-est-elle-bien-faite-surprend/dp/B01DTSFFWW)

Laubu C. et Dechaume-Moncharmont F.-X. (2016) Comment être heureux et avoir beaucoup d'enfants quand on est un poisson, _The Conversation_, 16 mars 2016

Dechaume-Moncharmont F.-X. et Pham-Delègue M.-H. (2002) L'évaluation de la distance chez les abeilles. Universalia 2002, _Encyclopædia Universalis_, Paris, p.288

Azzouz H. et  Dechaume-Moncharmont F.-X. (2000) La communication sociale chez l'abeille *Apis mellifera* L. _Bulletin Technique Apicole_ 27:9-19
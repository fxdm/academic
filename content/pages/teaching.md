---
title: "Teaching"
date: 2022-11-28T14:02:19+01:00
draft: false
---

####  2024-2025
- Biodiversity, L1 Divi univ Lyon (BSc, 1st year)
- Numerical ecology, L2 univ Lyon (BSc, 2d year)
- Animal behaviour, L3 ENS Lyon (BSc, 3d year)
- Behavioural ecology, M1 BEE univ Lyon (MSc, 4th year)
- Game theory, M1 ENS Lyon (MSc, 4th year)

#### Previous years
- Animal behaviour, L3 (BSc, 3d year)
- Behavioural ecology, M1 (MSc, 4th year)
- Behavioural ecology, M2 (MSc, 5th year)
- Game theory, M1 (MSc, 4th year)
- Mathematics for biologists, L1 (BSc, 1st year)
- Statistics, M2 (MSc, 5th year)
- C++ programming language, L3 (BSc, 3d year)

[![University of Lyon](/img/logo/ucbl.jpg)](https://www.univ-lyon1.fr/) [![UFR biosciences](/img/logo/ufr.jpg)](https://ufr-biosciences.univ-lyon1.fr/) [![Master BEE](/img/logo/bee.jpg)](https://h2olyon.universite-lyon.fr/master-bee-lyon-253656.kjsp) [![ENS Lyon](/img/logo/ens.jpg)](http://www.ens-lyon.fr/)

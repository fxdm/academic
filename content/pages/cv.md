---
title: "Short CV"
date: 2022-11-28T14:02:19+01:00
draft: false
---

|  Education     |   |   
| :---        |    :---    |  
| 1999:      | M.Sc Ecology, [Sorbonne Université](https://www.sorbonne-universite.fr/en) (formerly univ Paris VI Pierre-et-Marie-Curie), Paris, France  |     
| 2003:      | PhD Ecology, [Sorbonne Université](https://www.sorbonne-universite.fr/en) (formerly univ Paris VI Pierre-et-Marie-Curie), Paris, France [[details]](/pages/phd) |     
| 2014:       | Habilitation (HDR), university of Burgundy, Dijon, France [[details]](/pages/hdr)  |     
    
	
|  Positions  |   |   
| :---        |    :---    |  
| 1999-2003:      | PhD student at the Laboratory for Comparative Neurobiology of Invertebrates, Bures-sur-Yvette, France. Supervision: Minh-Hà Pham-Delègue and Odile Pons   |     
| 2004-2006:    | Post-doctoral position at the [University of Bristol](https://www.bristol.ac.uk/), United-Kingdom, in collaboration with Nigel Franks, Alasdair Houston and John McNamara   |     
| 2006-2019:       | Lecturer (maître de conférences) at the [university of Burgundy](https://www.u-bourgogne.fr/), Dijon, France. Member of the [UMR CNRS 6282 Biogéosciences](https://biogeosciences.u-bourgogne.fr/), Evolutionary Ecology group  |     
| from 2019:       | Professor at the [university Claude Bernard Lyon 1](https://www.univ-lyon1.fr/), France. Member of the [UMR CNRS 5023 LEHNA](https://umr5023.univ-lyon1.fr/), [Ecophysiology, behaviour and conservation group](http://umr5023.univ-lyon1.fr/equipes-de-recherche/e2c)  |     

[![Sorbonne université](/img/logo/su.png)](https://www.sorbonne-universite.fr/en) [![University of Bristol](/img/logo/bristol.jpg)](https://www.bristol.ac.uk/) [![ubfc](/img/logo/ubfc.png)](https://www.ubfc.fr/) [![University of Lyon](/img/logo/lyon1_s.jpg)](https://www.univ-lyon1.fr/)
---
title: ""
date: 2022-11-30T14:02:19+01:00
draft: false
---

[![CC-by-NC](/img/logo/CC_BY-NC.png)](https://creativecommons.org/licenses/by-nc/4.0/)\
Except where otherwise noted, content on this site is licensed under a [CC BY-NC](https://creativecommons.org/licenses/by-nc/4.0/) Creative Commons Attribution International license. 

[![hugo ](/img/logo/hugo_200.png)](https://gohugo.io/)\
This website is developed in [Hugo](https://gohugo.io/), an open source static site generator written in Go. The theme is adapted from [Arabica](https://github.com/nirocfz/arabica). Source code of the site is to be found on [gitlab](https://gitlab.com/fxdm/academic).

Privacy Matters. This website does not use any tracking technology, such as cookies, trackers, IP logging or analytics tools. In line with the European General Data Protection Regulation (GDPR), we do not collect, use or store any personal data from the users of this website. 